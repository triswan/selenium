# Practice

## Test Case 1: Login failed

1. Buka Selenium IDE: ![Welcome Window](img/welcome.png)
1. Klik **Record a new test in a new project** ![New Project Menu](img/record-a-new-test-in-a-new-project.png)
1. Isi **PROJECT NAME** dengan nama project (contoh: "**MMS**"), lalu klik OK ![Project Name Input](img/name-your-new-project.png)
1. Isi **BASE URL** dengan base url web yang akan ditest (contoh: **http://172.17.0.125/mms_atalian**), lalu klik **Start Recording** ![Base Url Input](img/set-your-project-base-url.png) 
1. Selenium IDE akan membuka browser/tab baru dan akan mengaktifkan mode merekam. Pada mode ini, selenium IDE akan merekam semua aktivitas pada browser yang dibuka tadi. Tanda Selenium sedang merekan adalah icon rekaman akan berkedip ![Recording Button](img/recording.gif) 
1. dan pada browser yang direkam akan menampilkan watermark **Selenium IDE is recording...** di kanan bawah ![Browser Recording Watermark](img/browser-recording.png)
1. Lakukan aktitifas pada browser tersebut contoh mengisi input **username** & **password** (coba isi dengan data yang salah untuk case negatif, contoh: **wrong@user.com**, passwod: **wrongpassword**) kemudian klik button **LOGIN**
1. Hasil rekaman dapat dilihat pada panel test case. ![Recorded Test Case](img/test-case.png)
1. Setelah browser selesai loading dan menampilkan hasil, klik kanan pada message yang ditampilkan di browser, kemudian pilih menu **Selenium IDE** > **Assert Text** ![Selenium IDE Menu](img/menu.png)
1. Pada poin ini rekaman dapat dihentikan dengan meng-klik tombol **Stop recording (Ctrl+U)** ![Stop Recording Button](img/stop-recording.png)
1. Tombol rekaman akan berhenti berkedip dan hasil dari pilihan menu Selenium IDE terlihat sudah terekam pada panel test case
![Finished Test Case](img/finished.png)
1. Untuk menjalankan test case yang sudah direkam tadi, klik **Run current test (Ctrl+R)** ![Run Current Test Button](img/run-current-test.png)
1. Selenium IDE akan menjalankan test case yang terpilih, dan hasilnya jika berwarna hijau berarti lulus, merah berarti tidak lulus ![Running Test Case](img/running.png)
1. Kita bisa me-rename test case (contoh: rename menjadi **Login failed**) dengan cara klik kanan menu di sebelah kanan item test case, lalu pilih **Rename** ![Rename Test Case menu](img/rename-test-case.png)
1. Jangan lupa untuk menyimpan project Selenium IDE kita dengan cara meng-klik tombol **Save project (Ctrl+S)** ![Save Project Button](img/save-project.png)

## Test Case 2: Login success

1. Buat test case baru dengan meng-klik tombol **Add new test** ![Add New Test Case Button](img/add-new-test-case.png)
1. Isi nama test case yang baru (contoh: **Login success**), lalu klik **ADD** ![New Test Case Name](img/new-test-case-name.png)
1. Klik tombol **Start recording** ![Start Recording Button](img/start-recording.png)
1. Buka halaman login, lalu isi user dan password dengan data yang benar. Browser akan masuk ke halaman dashboard. ![Dashboard](img/dashboard.png)
1. Pada latihan sebelumnya, assert dilakukan melalui klik kanan pada browser dan memilih menu Selenium IDE. Pada latihan kali ini, assert akan dilakukan dengan mengetik **assert text** pada input box **Command** ![Command Field](img/assert-text.png)
1. Klik tombol **Select target in page** ![Select Target button](img/select-target.png)
1. Kemudian klik pada nama user di bawah avatar, contoh Supervisor ![Avatar](img/avatar.png)
1. Selenium IDE akan menambahkan step pada test case panel ![](img/assert-text-target.png)
1. Dalam keadaan step **assert text** tersebut masih terpilih, ketik pada input box **Value**: nama user yang digunakan untuk login, contoh **Supervisor** ![](img/value.png)
1. Stop rekaman, lalu coba jalankan test case Login success yang baru saja dibuat. **Catatan**: Jangan lupa logout dulu pada browser untuk memastikan halaman berada pada posisi belum login
 ![](img/login-success.png)

## Test Case 3: Dashboard

Pada latihan ini kita akan mempelajari verifikasi / assert beberapa element

1. Buat test case dengan nama: **Dashboard**
1. Rekam test case, lalu buka halaman dashboard
1. Pada input box Command ketik: **verify element present** kemudian klik **Select target in page**, lalu klik salah satu box yang tampil di dashboard, contoh pada box chart
1. Ulangi menambah step untuk memverifikasi box-box lainnya dengan cara klik kanan pada bagian daftar step **Insert new command**. **Catatan**: Kita juga dapat meng-copy-paste step untuk mempercepat proses ![](img/insert-new-command.png)
1. Setelah selesai hasilnya seperti ini: ![](img/dashboard-steps.png)
1. Kita juga bisa menambahkan desripsi untuk setiap step agar lebih mudah diingat. Ketik pada input box **Description** untuk setiap step: ![](img/dashboard-steps-2.png)
1. Jalankan test case ![](img/dashboard-test.png)

Catatan:

- Dengan assert, jika step tidak lulus, maka test case akan berhenti. Tetapi dengan verify akan dilanjutkan ke step berikutnya.
- veriry element present hanya akan menguji keberadan element html pada laman web, tidak diuji isi / teks nya

## Test Case 4: Create Client - Empty

Silakan buat test case untuk membuat data client baru yang inputannya nya kosong semua (negative test) berdasarkan yang sudah dipelajari. Contoh test case dapat dilihat: ![](img/client-empty.png)

## Test Case 5: Create Client - Success

Silakan buat test case untuk membuat data client baru (positive test). Contoh tidak disertakan

## Organizing Test Cases

Untuk mengelola test case yang sudah banyak, kita dapat mengelompokannya menjadi beberapa Test Suite. 

- Klik pada combo box, pilih **Test suites (Ctrl+2)**: ![](img/test-suite.png)
 
- Klik pada menu di sebelah kanan Test suite 

![](img/test-suite-menu.png) ![](img/test-suite-menu-popup.png)
 
- Dari menu, pilih  (**Add tests**) untuk menambah test case ke dalam test suite terpilih, contoh ![](img/add-test-case-to-suite.png)

- Dari menu kita juga dapat memilih **Settings** untuk mengubah pengaturan seperti timeout ![](img/suite-settings.png)

- Untuk mengurut test case, drag & drop test case yang ada di list ![](img/re-order.png)

- Dengan keadaan terurut, kita dapat menjalankan Test suite sesuai urutan. Karena pada tampilan Tests (Ctrl+1), urutan test case hanya berdasarkan alfabet dan tidak dapat diurutkan ![](img/run-all.png)

## Conclusion

Silakan explore lebih lanjut mengenai Selenium IDE untuk lebih mengoptimalkan lagi penggunaannya. Semoga bermanfaat buat teman-teman semua. 

Selamat bereksplorasi!
